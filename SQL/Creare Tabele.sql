drop database if exists warehouse;
create database if not exists warehouse;
use warehouse;

drop table if exists customer;
create table if not exists customer
(id int unique auto_increment primary key,
name varchar(30),
address varchar(45));

drop table if exists product;
create table if not exists product
(id int unique auto_increment primary key,
name varchar(40),
quantity float,
price float);

drop table if exists orders;
create table if not exists orders
(id int unique auto_increment primary key,
customerId int);

drop table if exists OrderProduct;
create table if not exists OrderProduct
(id int unique auto_increment primary key,
productId int,
orderId int,
quantity float,
cost float);

alter table orders
add constraint fk_customer_order
foreign key (customerId) references customer(id)
on delete cascade
on update cascade;

alter table OrderProduct
add constraint fk_OrderProduct_product
foreign key (productId) references product(id)
on delete cascade
on update cascade;

alter table OrderProduct
add constraint fk_OrderProduct_order
foreign key (orderId) references orders(id)
on delete cascade
on update cascade;