package Model;

/**
 * Transpunerea in cod unei comenzi de produse
 * Atributele sunt cantitatea comandata, costul total al comenzii si id-ul produsului si al comnezii efectuata de client
 * Folosita pentru a sparge relatiile de many to many din baza de date a aplicatiei
 * Atributele sunt private deci au nevoie de getters si stters pentru a putea fi accesate
 */

public class OrderProduct {

    private int id;
    private int productId;
    private int orderId;
    private float quantity;
    private float cost;

    public OrderProduct(int id, int productId, int orderId, float quantity, float cost) {
        this.id = id;
        this.productId = productId;
        this.orderId = orderId;
        this.quantity = quantity;
        this.cost = cost;
    }

    public OrderProduct() {

    }

    /**
     * Getter pentru atributul id
     * @return int - id-ul
     */

    public int getId() {
        return this.id;
    }

    /**
     * Setter pentru atributul id
     * Se actualizeaza id-ul
     * @param id - noul id
     */

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Getter pentru atributul productId
     * @return int - id-ul produsului din comanda
     */

    public int getProductId() {
        return this.productId;
    }

    /**
     * Setter pentru atributul productId
     * Se actualizeaza id-ul produsului
     * @param productId - noul id
     */

    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     * Getter pentru id-ul comenzii efecutate de client
     * @return int - id-ul comenzii
     */

    public int getOrderId() {
        return this.orderId;
    }

    /**
     * Setter pentru id-ul comezii efectuate de client
     * Se actualizeaza valoarea id-ului
     * @param orderId - noul id
     */

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     * Getter pentru cantitate
     * @return float - cantitatea comenzii
     */

    public float getQuantity() {
        return this.quantity;
    }

    /**
     * Setter pentru cantitate
     * Se actualizeaaza cantitatea
     * @param quantity - noua cantitate
     */

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    /**
     * Getter pentru cost
     * @return float - pretul comenzii
     */

    public float getCost() {
        return this.cost;
    }

    /**
     * Setter pentru pretul comezii
     * Se actualizeaza pretul comezii
     * @param cost - noust pret
     */

    public void setCost(float cost) {
        this.cost = cost;
    }
}
