package Model;

/**
 * Transpunerea in cod a unei comeni efectuate de catre client
 * Contine doar id-ul unic al comenzii si id-ul clientului care efectueaza comanda
 * Atributele sunt private, pentru accesare este nevoie de getters si setters
 */

public class Orders {

    private int id;
    private int customerId;

    public Orders(int id, int customerId) {
        this.id = id;
        this.customerId = customerId;
    }

    public Orders() {

    }

    /**
     * Getter pentru id-ul comezii
     * Se returneaza id-ul comenzii
     * @return
     */

    public int getId() {
        return this.id;
    }

    /**
     * Setter pentru id
     * Se actualizaeaza valoarea id-ului cu noua valoare primita ca si parametru
     * @param id
     */

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Getter pentru id-ul clientului
     * Se returneza id-ul clientului care a efectuat comanda
     * @return
     */

    public int getCustomerId() {
        return this.customerId;
    }

    /**
     * Setter pentru id-ul clientului
     * Se actualizeaza id-ul clientului la noua valoare primita ca si parametru
     * @param customerId
     */

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

}
