package Model;

/**
 * Transpunerea in cod a unui produs din lumea reala.
 * Atributele clasei sunt private, deci pentru acceesarea lor este nevoie de metode de tip getter si setter.
 */

public class Product {

    private int id;
    private String name;
    private float quantity;
    private float price;

    public Product(int id, String name, float quantity, float price) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public Product() {

    }

    /**
     * Getter pentru id-ul produsului
     * Se returneaza id-ul produsului
     * @return
     */

    public int getId() {
        return this.id;
    }

    /**
     * Setter pentru id-ul produsului
     * Se actualizeaza id-ul cu noua valoare primita ca si parametru
     * @param id
     */

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Getter pentru numele produsului
     * Se returneza numele produsului
     * @return
     */

    public String getName() {
        return this.name;
    }

    /**
     * Setter pentru numele produslui
     * Se schimba numele produslui in noul nume, primit ca si parametru
     * @param name
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter pentru cantitatea disponibila a produsului
     * Se returneaza cantitatea disponibila
     * @return
     */

    public float getQuantity() {
        return this.quantity;
    }

    /**
     * Setter pentru cantitatea produsului
     * Se schimba cantitatea disponibila in noua catintate primita ca si parametru
     * @param quantity
     */

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    /**
     * Getter pentru pretul produsului
     * Se returneaza pretul produsului
     * @return
     */

    public float getPrice() {
        return this.price;
    }

    /**
     * Setter pentru pretul produsului
     * Se schimba pretul produslui la noua valoare primita ca si parametru
     * @param price
     */

    public void setPrice(float price) {
        this.price = price;
    }

    /**
     * Se afiseaza sub forma de text toate detaliile legate de produs.
     * @return
     */

    @Override
    public String toString() {
        return "Product: " + this.getName() + "; quantity: " + this.getQuantity() + "; price: " + this.getPrice();
    }
}
