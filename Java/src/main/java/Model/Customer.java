package Model;

/**
 * Transpunearea in cod a unui clinent din lumea reala.
 * Ca atribute definitorii pentru client putem enumera numele si adresa, dar se pot adauga si altele.
 * Atributele clasei sunt private, deci va fi nevoie sa fie accesate prin getters and setters.
 */

public class Customer {

    private int id;
    private String name;
    private String address;

    public Customer(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public Customer() {

    }

    /**
     * Getter-ul pentru atributul ID.
     * @return int - id-ul clientului.
     */

    public int getId() {
        return this.id;
    }

    /**
     * Setter-ul pentru artibutul ID.
     * @param id
     */

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Getter-ul atributului name
     * @return String - numele clientului
     */

    public String getName() {
        return this.name;
    }

    /**
     * Setter-ul atributului name
     * Se actualizaeaza numele clientului ce apeleaza metoda
     * @param name - noul nume al clientului ce apeleaza metoda
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter-ul atributului address
     * @return String - adresa clientului care apeleaza metoda
     */

    public String getAddress() {
        return this.address;
    }

    /**
     * Setter-ul atributului address
     * Se actualizeaza adresa clientului care apeleaza materia
     * @param address - noua adresa a clientului
     */

    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Metoda folosita pentru afisarea tuturor datelor legate de client sub forma unui text.
     * @return
     */

    @Override
    public String toString() {
        return "Customer: " + this.getName() + "; Address: " + this.getAddress();
    }
}
