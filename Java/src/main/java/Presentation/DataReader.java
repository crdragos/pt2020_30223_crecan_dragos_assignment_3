package Presentation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Clasa DataReadar are rolul de a citi datele introduse de catre utilizator in fisierul de intrare
 * Singurul atribut al clasei este fisierul de intrare
 */

public class DataReader {

    public DataReader() {

    }

    /**
     * Aceasta metoda citeste cate o linie din fisierul de intrare, dupa care o imparte dupa anumiti separatori
     * Dupa ce datele au fost prelucrate se vor transmite mai departe clasei care va transforma input-ul intr-un query pentru baza de date
     * @param file
     */

    public void readData(File file) {
        try {
            Scanner input = new Scanner(file);
            DataManipulation dataManipulation = new DataManipulation();

            while (input.hasNext()) {
                String[] line = input.nextLine().split("[,:]");
                for (int i = 0; i < line.length; i++) {
                    if (line[i].charAt(0) == ' ') {
                        line[i] = line[i].substring(1);
                    }
                }
                dataManipulation.chooseTable(line);
            }
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
    }
}
