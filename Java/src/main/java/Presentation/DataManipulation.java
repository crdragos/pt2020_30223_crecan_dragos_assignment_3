package Presentation;

import BLL.CustomerService;
import BLL.OrderProductService;
import BLL.OrderService;
import BLL.ProductService;
import Model.Customer;
import Model.OrderProduct;
import Model.Orders;
import Model.Product;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

/**
 * Clasa DataManipulation va stabili ce fel de instructiuni trebuie executate in functie de datele primite din fisier
 */

public class DataManipulation {

    private CustomerService customerService;
    private ProductService productService;
    private OrderService orderService;
    private OrderProductService orderProductService;

    private int understockIndex;
    private int billIndex;
    private int reportIndex;

    private int ordersIndex;

    // private static final String PATH = "C:\\Users\\40752\\Desktop\\PDF\\";
    private static final String PATH = "PDF";
    private static final Font fontPDF = FontFactory.getFont(FontFactory.TIMES_ROMAN, 18, BaseColor.BLACK);

    public DataManipulation() {
        this.customerService = new CustomerService();
        this.productService = new ProductService();
        this.orderService = new OrderService();
        this.orderProductService = new OrderProductService();
        this.understockIndex = 0;
        this.billIndex = 0;
        this.reportIndex = 0;
        this.ordersIndex = 0;
    }

    /**
     * Metoda de gestionare a clientilor
     * In functie de instructiunea primita ca parametru, se va transmite catre baza de date ce fel de instructiune trebuie executat in baza de date
     * @param instruction
     */

    public void handleCustomers(String[] instruction) {
        String command = instruction[0];

        if (command.contains("Insert")) {
            String name = instruction[1];
            String address = instruction[2];
            this.customerService.add(new Customer(0, name, address));
        } else if (command.contains("Delete")) {
            String name = instruction[1];
            Customer toDeleteCustomer = this.customerService.getByName(name);
            if (toDeleteCustomer != null) {
                this.customerService.delete(toDeleteCustomer.getId());
            } else {
                throw new NoSuchElementException("There is no client with name: " + name + ".");
            }
        } else if (command.contains("Report")) {
            generateReport("Customer");
        }
    }

    /**
     * Metoda de gestionare a produselor
     * In functie de instructiunea primita ca parametru, se va transmite catre baza de date ce fel de instructiune trebuie executat in baza de date
     * @param instruction
     */

    public void handleProducts(String[] instruction) {
        String command = instruction[0];

        if (command.contains("Insert")) {
            String name = instruction[1];
            float quantity = Float.parseFloat(instruction[2]);
            float price = Float.parseFloat(instruction[3]);
            Product toUpdateProduct = this.productService.getByName(name);
            if (toUpdateProduct == null) {
                this.productService.add(new Product(0, name, quantity, price));
            } else {
                toUpdateProduct.setQuantity(toUpdateProduct.getQuantity() + quantity);
                this.productService.update(toUpdateProduct, toUpdateProduct.getId());
            }
        } else if (command.contains("Delete")) {
            String name = instruction[1];
            Product toDeleteProduct = this.productService.getByName(name);
            if (toDeleteProduct != null) {
                productService.delete(toDeleteProduct.getId());
            } else {
                throw new NoSuchElementException("There is no product with name: " + name + ".");
            }
        } else if (command.contains("Report")) {
            generateReport("Product");
        }
    }

    /**
     * Metoda de gestionare a comenzilor
     * In functie de instructiunea primita ca parametru, se va transmite catre baza de date ce fel de instructiune trebuie executat in baza de date
     * @param instruction
     */

    public void handleOrders(String[] instruction) {
        String command = instruction[0];

        if (command.contains("Report")) {
            generateReport("Order");
        } else {
            String customerName = instruction[1];
            String productName = instruction[2];
            Float orderedQuantity = Float.parseFloat(instruction[3]);

            Customer customer = customerService.getByName(customerName);
            if (customer == null) {
                throw new NoSuchElementException("There is no customer with name: " + customerName + ".");
            }

            Product product = productService.getByName(productName);
            if (product == null) {
                throw new NoSuchElementException("There is no product with name: " + productName + ".");
            }

            if (product.getQuantity() - orderedQuantity < 0) {
                generateUnderstockPDF(product, orderedQuantity);
            } else {
                generateBill(customer, product, orderedQuantity);
                product.setQuantity(product.getQuantity() - orderedQuantity);
                productService.update(product, product.getId());
                orderService.add(new Orders(0, customer.getId()));
                this.ordersIndex++;
                orderProductService.add(new OrderProduct(0, product.getId(), this.ordersIndex, orderedQuantity, product.getPrice() * orderedQuantity));
            }
        }
    }

    /**
     * Metoda care va alege asupra carei tabele se vor executa operatiile din baza de date
     * In functie de alegera facuta se vor apela metodele corespunzatoare entiatii alese
     * @param instruction
     */

    public void chooseTable(String[] instruction) {
        String command = instruction[0].toLowerCase();
        if (command.contains("client")) {
            handleCustomers(instruction);
        } else if (command.contains("product")) {
            handleProducts(instruction);
        } else if (command.contains("order")) {
            handleOrders(instruction);
        }
    }

    /**
     * Generarea PDF - ului care va contine mesajul de "stoc insuficient"
     * Metoda primeste ca si parametrii produsul comandat si cantitatea comandata, pentru a face mesajul destul de explicit
     * @param product
     * @param quantity
     */

    public void generateUnderstockPDF(Product product, float quantity) {
        this.understockIndex++;

        Document document = new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream(PATH + "Understock " + this.understockIndex + ".pdf"));
            document.open();

            Paragraph title = new Paragraph("UNDERSTOCK: ", fontPDF);
            title.setAlignment(Element.ALIGN_CENTER);
            document.add(title);

            Paragraph productName = new Paragraph("Ordered product : " + product.getName(), fontPDF);
            document.add(productName);

            Paragraph existentQuantity = new Paragraph("Existent quantity : " + product.getQuantity(), fontPDF);
            document.add(existentQuantity);

            Paragraph orderedQuantity = new Paragraph("Ordered quantity : " + quantity, fontPDF);
            document.add(orderedQuantity);

            document.close();
        } catch (DocumentException documentException) {
            documentException.printStackTrace();
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
    }

    /**
     * Metoda are ca scop generarea unui document PDF care sa reprezinte bonul primit de catre clinet in momentul efectuarii unei comenzi.
     * Parametrii primiti in semnatura metodei reprezinta clientul, produsul si cantitatea comandata
     * @param customer
     * @param product
     * @param oreredQuantity
     */

    public void generateBill(Customer customer, Product product, float oreredQuantity) {
        this.billIndex++;

        Document document = new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream(PATH + "Bill " + this.billIndex + ".pdf"));
            document.open();

            Paragraph title = new Paragraph("Bill", fontPDF);
            title.setAlignment(Element.ALIGN_CENTER);
            document.add(title);

            Paragraph customerParagraph = new Paragraph("Customer: " + customer.getName(), fontPDF);
            document.add(customerParagraph);

            Paragraph productParagraph = new Paragraph("Product: " + product.getName() + "( " + product.getPrice() + " / buc)", fontPDF);
            document.add(productParagraph);

            Paragraph totalParagraph = new Paragraph("Total: " + (product.getPrice() * oreredQuantity), fontPDF);
            document.add(totalParagraph);

            document.close();
        } catch (DocumentException documentException) {
            documentException.printStackTrace();
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
    }

    /**
     * Metoda va genera un document PDF ce va contine un tabel asemator cu cel din SQL atunci cand se executa operatia "SELECT * FORM ... "
     * Parametru primit de catre metoda va decide pentru care tabela din baza de date sa va face raport, deoarece el difera de la o tabela la alta.
     * @param entityName
     */

    public void generateReport(String entityName) {
        this.reportIndex++;

        Document document = new Document();
        try {

            PdfWriter.getInstance(document, new FileOutputStream(PATH + "Report " + this.reportIndex + " - " + entityName + ".pdf"));
            document.open();

            if (entityName.equalsIgnoreCase("Customer")) {
                Paragraph title = new Paragraph("Report (CLIENT)", fontPDF);
                title.setAlignment(Element.ALIGN_CENTER);
                document.add(title);
                document.add(new Paragraph("\n\n"));

                PdfPTable pdfPTable = new PdfPTable(3);
                addTableHeader(pdfPTable, entityName);
                addRows(pdfPTable, entityName);
                document.add(pdfPTable);
            } else if (entityName.equalsIgnoreCase("Product")) {
                Paragraph title = new Paragraph("Report (PRODUCT)", fontPDF);
                title.setAlignment(Element.ALIGN_CENTER);
                document.add(title);
                document.add(new Paragraph("\n\n"));

                PdfPTable pdfPTable = new PdfPTable(4);
                addTableHeader(pdfPTable, entityName);
                addRows(pdfPTable, entityName);
                document.add(pdfPTable);
            } else if (entityName.equalsIgnoreCase("Order")) {
                Paragraph title = new Paragraph("Report (ORDER)", fontPDF);
                title.setAlignment(Element.ALIGN_CENTER);
                document.add(title);
                document.add(new Paragraph("\n\n"));

                PdfPTable pdfPTable = new PdfPTable(5);
                addTableHeader(pdfPTable, entityName);
                addRows(pdfPTable, entityName);
                document.add(pdfPTable);
            }

            document.close();

        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (DocumentException documentException) {
            documentException.printStackTrace();
        }
    }

    /**
     * Metoda folosita la construirea capului de tabel din fiecare raport
     * Parametrii primiti de catre metoda sunt tabelui pentru care se creeaza capul de tabel si entitatea corespunzatoare
     * @param table
     * @param descriptor
     */

    private void addTableHeader(PdfPTable table, String descriptor) {
        if (descriptor.equalsIgnoreCase("Customer")) {
            Stream.of("ID", "Name", "Address")
                    .forEach(columnTitle -> {
                        PdfPCell header = new PdfPCell();
                        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                        header.setBorderWidth(3);
                        header.setPhrase(new Phrase(columnTitle));
                        table.addCell(header);
                    });
        } else if (descriptor.equalsIgnoreCase("Product")) {
            Stream.of("ID", "Name", "Quantity", "Price")
                    .forEach(columnTitle -> {
                        PdfPCell header = new PdfPCell();
                        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                        header.setBorderWidth(3);
                        header.setPhrase(new Phrase(columnTitle));
                        table.addCell(header);
                    });
        } else if (descriptor.equalsIgnoreCase("Order")) {
            Stream.of("ID", "Customer Name", "Product Name", "Quantity", "Total Price")
                    .forEach(columnTitle -> {
                        PdfPCell header = new PdfPCell();
                        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                        header.setBorderWidth(3);
                        header.setPhrase(new Phrase(columnTitle));
                        table.addCell(header);
                    });
        }
    }

    /**
     * Metoda de adaugare a datelor in tabel
     * Parametrii metodei reprezinta tabelul in care se vor adauga datele si enitatea corespunzatoare datelor ce trebuie introduse
     * @param table
     * @param entityName
     */

    private void addRows(PdfPTable table, String entityName) {
        if (entityName.equalsIgnoreCase("Customer")) {
            ArrayList<Customer> customers = this.customerService.get();
            for (Customer customer : customers) {
                table.addCell(Integer.toString(customer.getId()));
                table.addCell(customer.getName());
                table.addCell(customer.getAddress());
            }
        } else if (entityName.equalsIgnoreCase("Product")) {
            ArrayList<Product> products = this.productService.get();
            for (Product product : products) {
                table.addCell(Integer.toString(product.getId()));
                table.addCell(product.getName());
                table.addCell(Float.toString(product.getQuantity()));
                table.addCell(Float.toString(product.getPrice()));
            }
        } else if (entityName.equalsIgnoreCase("Order")) {
            ArrayList<Orders> orders = this.orderService.get();
            ArrayList<OrderProduct> orderProducts = this.orderProductService.get();
            for (int i = 0; i < orders.size(); i++) {
                table.addCell(Integer.toString(orders.get(i).getId()));

                Customer customer = this.customerService.getById(orders.get(i).getCustomerId());
                table.addCell(customer.getName());

                Product product = this.productService.getById(orderProducts.get(i).getProductId());
                table.addCell(product.getName());

                table.addCell(Float.toString(orderProducts.get(i).getQuantity()));
                table.addCell(Float.toString(orderProducts.get(i).getCost()));
            }
        }
    }

}
