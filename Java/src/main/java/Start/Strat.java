package Start;

import Presentation.DataReader;

import java.io.File;

/**
 * Clasa start contine metoda main, cea care va fi executata la rularea aplicatiei
 */

public class Strat {
    public static void main(String[] args) {

        DataReader dataReader = new DataReader();
        dataReader.readData(new File(args[0]));
    }
}
