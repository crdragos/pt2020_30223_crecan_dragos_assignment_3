package BLL;

import BLL.validators.CustomerAddressValidator;
import BLL.validators.CustomerNameValidator;
import BLL.validators.Validator;
import DAL.CustomerRepository;
import Model.Customer;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Service-ul clasei Client
 * Aici vor fi apelate metodele din repository si se vor aplica validari asupra datelor de urmeaza sa fie stocate in baza de date.
 * Ca si atrubute avem o lista de validatori si o instanta a clasei repository
 */

public class CustomerService {

    private List<Validator<Customer>> validators;
    private CustomerRepository customerRepository = new CustomerRepository();

    public CustomerService() {
        validators = new ArrayList<Validator<Customer>>();
        validators.add(new CustomerNameValidator());
        validators.add(new CustomerAddressValidator());
    }

    /**
     * Se face o cautare dupa id-ul primit ca si parametru, in tabela de client
     * Se returneaza clientul cu id-ul dat ca si parametru, sau se arunca o exceptie in cazul in care acesta nu exista
     * @param id
     * @return
     */

    public Customer getById(int id) {
        Customer customer = this.customerRepository.getById(id);

        if (customer == null) {
            throw new NoSuchElementException("The customer with id = " + id + " was not found!");
        }

        return customer;
    }

    /**
     * Se executa o cautare dupa nume tabela client
     * Se returneaza clientul care are numele primit ca si parametru, iar in cazul in care nu exista un astfel de client se arunca o exceptie
     * @param name
     * @return
     */

    public Customer getByName(String name) {
        Customer customer = this.customerRepository.getByName(name);

        if (customer == null) {
            throw new NoSuchElementException("The customer with name: " + name + " was not found!");
        }

        return customer;
    }

    /**
     * Se returneaza toti clientii din baza de date
     * @return
     */

    public ArrayList<Customer> get() {
        ArrayList<Customer> customers = this.customerRepository.get();

        if (customers == null) {
            throw new NoSuchElementException("There are no customers!");
        }

        return customers;
    }

    /**
     * Metoda de adaugare a unui client, dat ca si parametru, in tabela de clienti a bazei de date
     * Metoda retuneaza id-ul clientului inserat
     * @param customer
     * @return
     */

    public int add(Customer customer) {
        for (Validator<Customer> validator : validators) {
            validator.validate(customer);
        }

        return this.customerRepository.add(customer);
    }

    /**
     * Metoda de actualizare a clientului cu id-ul primit ca si parametru
     * Toate datele respectivului client vor fi schimbate in datele clientului primit ca si parametru
     * Se returneaza statusul efectuarii operatiei
     * @param customer
     * @param id
     * @return
     */

    public boolean update(Customer customer, int id) {
        return this.customerRepository.update(customer, id);
    }

    /**
     * Metoda de stergere a clientului cu id-ul dat ca si parametru
     * Se returneaza statusul efectuarii operatiei
     * @param id
     * @return
     */

    public boolean delete(int id) {
        return this.customerRepository.delete(id);
    }
}
