package BLL;

import BLL.validators.ProductNameValidator;
import BLL.validators.ProductPriceValidator;
import BLL.validators.ProductQuantityValidator;
import BLL.validators.Validator;
import DAL.ProductRepository;
import Model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Service-ul clasei Product
 * Aici vor fi apelate metodele din repository si se vor aplica validari asupra datelor de urmeaza sa fie stocate in baza de date.
 * Ca si atrubute avem o lista de validatori si o instanta a clasei repository
 */

public class ProductService {

    private List<Validator<Product>> validators;
    private ProductRepository productRepository = new ProductRepository();

    public ProductService() {
        this.validators = new ArrayList<>();
        validators.add(new ProductQuantityValidator());
        validators.add(new ProductNameValidator());
        validators.add(new ProductPriceValidator());
    }

    /**
     * Se face o cautare dupa id-ul primit ca si parametru, in tabela de produse
     * Se returneaza produsul cu id-ul dat ca si parametru, sau se arunca o exceptie in cazul in care acesta nu exista
     * @param id
     * @return
     */

    public Product getById(int id) {
        Product product = productRepository.getById(id);

        if (product == null) {
            throw new NoSuchElementException("The product with id: " + id + " was not found!");
        }

        return product;
    }

    /**
     * Se executa o cautare dupa nume in tabela produs
     * Se returneaza produsul care are numele primit ca si parametru, iar in cazul in care nu exista un astfel de client se arunca o exceptie
     * @param name
     * @return
     */

    public Product getByName(String name) {
        Product product = productRepository.getByName(name);

        if (product == null) {
            //  throw new NoSuchElementException("The product with name " + productName + " was not found!");
            return null;
        }

        return product;
    }

    /**
     * Se returneaza toate produsele din baza de date
     * @return
     */

    public ArrayList<Product> get() {
        ArrayList<Product> products = productRepository.get();

        if (products == null) {
            throw new NoSuchElementException("There are no products!");
        }

        return products;
    }

    /**
     * Metoda de adaugare a unui produs, dat ca si parametru, in tabela de produse a bazei de date
     * Metoda retuneaza id-ul produsului inserat
     * @param product
     * @return
     */

    public int add(Product product) {
        for (Validator<Product> validator : validators) {
            validator.validate(product);
        }

        return productRepository.add(product);
    }

    /**
     * Metoda de actualizare a produsului cu id-ul primit ca si parametru
     * Toate datele respectivului produs vor fi schimbate in datele produsului primit ca si parametru
     * Se returneaza statusul efectuarii operatiei
     * @param product
     * @param id
     * @return
     */

    public boolean update(Product product, int id) {
        return productRepository.update(product, id);
    }

    /**
     * Metoda de stergere a produsului cu id-ul dat ca si parametru
     * Se returneaza statusul efectuarii operatiei
     * @param id
     * @return
     */

    public boolean delete(int id) {
        return productRepository.delete(id);
    }
}
