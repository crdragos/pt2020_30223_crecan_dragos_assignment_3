package BLL;

import BLL.validators.OrderProductQuantityValidator;
import BLL.validators.Validator;
import DAL.OrderProductRepository;
import Model.OrderProduct;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Service-ul clasei OrderProduct
 * Aici vor fi apelate metodele din repository si se vor aplica validari asupra datelor de urmeaza sa fie stocate in baza de date.
 * Ca si atrubute avem o lista de validatori si o instanta a clasei repository
 */

public class OrderProductService {

    private List<Validator<OrderProduct>> validators;
    private OrderProductRepository orderProductRepository = new OrderProductRepository();

    public OrderProductService() {
        validators = new ArrayList<>();
        validators.add(new OrderProductQuantityValidator());
    }

    /**
     * Se face o cautare dupa id-ul primit ca si parametru, in tabela orderProduct
     * Se returneaza order-ul cu id-ul dat ca si parametru, sau se arunca o exceptie in cazul in care acesta nu exista
     * @param id
     * @return
     */

    public OrderProduct getById(int orderProductId) {
        OrderProduct orderProduct = orderProductRepository.getById(orderProductId);

        if (orderProduct == null) {
            throw new NoSuchElementException("Product order with id = " + orderProductId + " was not found!");
        }

        return orderProduct;
    }

    /**
     * Se returneaza toate datele din tabela OrderProduct a bazei de date
     * @return
     */

    public ArrayList<OrderProduct> get() {
        ArrayList<OrderProduct> orderProducts = orderProductRepository.get();

        if (orderProducts == null) {
            throw new NoSuchElementException("There ar no order products.");
        }

        return orderProducts;
    }

    /**
     * Metoda de adaugare a unei noi comezi, data ca si parametru, in tabela OrderProduct a bazei de date
     * Metoda retuneaza id-ul comenzii inserate
     * @param orderProduct
     * @return
     */

    public int add(OrderProduct orderProduct) {
        for (Validator<OrderProduct> validator : validators) {
            validator.validate(orderProduct);
        }

        return orderProductRepository.add(orderProduct);
    }

    /**
     * Metoda de actualizare a comenzii cu id-ul primit ca si parametru
     * Toate datele respectivei comenzi vor fi schimbate in datele comenzii primite ca si parametru
     * Se returneaza statusul efectuarii operatiei
     * @param orderProduct
     * @param id
     * @return
     */

    public boolean update(OrderProduct orderProduct, int id) {
        return orderProductRepository.update(orderProduct, id);
    }

    /**
     * Metoda de stergere a comenzii cu id-ul dat ca si parametru
     * Se returneaza statusul efectuarii operatiei
     * @param id
     * @return
     */

    public boolean delete(int id) {
        return orderProductRepository.delete(id);
    }
}
