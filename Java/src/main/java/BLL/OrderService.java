package BLL;

import DAL.OrderRepository;
import Model.Orders;

import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * Service-ul clasei Oreder
 * Aici vor fi apelate metodele din repository
 * Ca si atrubute avem o instanta a clasei repository
 */

public class OrderService {

    OrderRepository orderRepository = new OrderRepository();

    public OrderService() {

    }

    /**
     * Se face o cautare dupa id-ul primit ca si parametru, in tabela orderProduct
     * Se returneaza order-ul cu id-ul dat ca si parametru, sau se arunca o exceptie in cazul in care acesta nu exista
     * @param id
     * @return
     */

    public Orders getById(int orderId) {
        Orders orders = orderRepository.getById(orderId);

        if (orders == null) {
            throw new NoSuchElementException("Order with id = " + orderId + " was not found!");
        }

        return orders;
    }

    /**
     * Se returneaza toate datele din tabela Order a bazei de date
     * @return
     */

    public ArrayList<Orders> get() {
        ArrayList<Orders> orders = orderRepository.get();

        if (orders == null) {
            throw new NoSuchElementException("There are no orders!");
        }

        return orders;
    }

    /**
     * Metoda de adaugare a unei comenzi, data ca si parametru, in tabela Orders a bazei de date
     * Metoda retuneaza id-ul comenzii inserate
     * @param orders
     * @return
     */

    public int add(Orders orders) {
        return orderRepository.add(orders);
    }

    /**
     * Metoda de actualizare a comenzii cu id-ul primit ca si parametru
     * Toate datele respectivei comenzi vor fi schimbate in datele comenzii primite ca si parametru
     * Se returneaza statusul efectuarii operatiei
     * @param orders
     * @param id
     * @return
     */

    public boolean update(Orders orders, int id) {
        return orderRepository.update(orders, id);
    }

    /**
     * Metoda de stergere a comenzii cu id-ul dat ca si parametru
     * Se returneaza statusul efectuarii operatiei
     * @param id
     * @return
     */

    public boolean delete(int id) {
        return orderRepository.delete(id);
    }
}
