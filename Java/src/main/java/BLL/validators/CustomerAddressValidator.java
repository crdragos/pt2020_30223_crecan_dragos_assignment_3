package BLL.validators;

import Model.Customer;

import java.util.regex.Pattern;

/**
 * Aceasta clasa are rolul de valida adresa pe care utilizatorul inearca sa o introduca in baza de date
 * Validarea se va face prin compararea valorii pe care dorim sa o inseram cu un pattern, pentru a nu putea introduce anumite caractere speciale
 * Se verifica si lungimea adresei intoduse, pentru a nu depasi lungimea acceptata in baza de date
 */

public class CustomerAddressValidator implements Validator<Customer> {

    private static final String ADDRESS_PATTERN = "^[a-z, A-Z.-0-9]+$";

    @Override
    public void validate(Customer customer) {
        if (customer.getAddress().length() <= 45) {
            Pattern pattern = Pattern.compile(ADDRESS_PATTERN);
            if (!pattern.matcher(customer.getAddress()).matches()) {
                throw new IllegalArgumentException("Invalid customed address: some special characters are not allowed");
            }
        } else {
            throw new IllegalArgumentException("Invalid customer address: too long");
        }
    }
}
