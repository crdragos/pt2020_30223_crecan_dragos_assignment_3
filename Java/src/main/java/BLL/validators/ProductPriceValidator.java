package BLL.validators;

import Model.Product;

/**
 * Aceasta clasa are rolul de valida pretul unui anumit produs pe care utilizatorul inearca sa il introduca in baza de date
 * Validarea se va face prin simpla verificare a cantitatii, aceasta trebuie neaparat sa fie mai mare ca 0
 */

public class ProductPriceValidator implements Validator<Product> {

    @Override
    public void validate(Product product) {
        if (product.getPrice() < 0) {
            throw new IllegalArgumentException("Invalid product price: price must be >= 0");
        }
    }
}
