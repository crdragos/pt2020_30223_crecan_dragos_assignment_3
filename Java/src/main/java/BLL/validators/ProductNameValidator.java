package BLL.validators;

import Model.Product;

import java.util.regex.Pattern;

/**
 * Aceasta clasa are rolul de valida numele produsului pe care utilizatorul inearca sa il introduca in baza de date
 * Validarea se va face prin compararea valorii pe care dorim sa o inseram cu un pattern, pentru a nu putea introduce caractere speciale sau cifre
 * Se verifica si lungimea numelui intodus, pentru a nu depasi lungimea acceptata in baza de date
 */

public class ProductNameValidator implements Validator<Product> {

    private static final String NAME_PATTERN = "^[a-z A-z]+$";

    @Override
    public void validate(Product product) {
        if (product.getName().length() < 30) {
            Pattern pattern = Pattern.compile(NAME_PATTERN);
            if (!pattern.matcher(product.getName()).matches()) {
                throw new IllegalArgumentException("Invalid product name: special characters and digits are not allowed");
            }
        } else {
            throw new IllegalArgumentException("Invalid product name:  too long");
        }
    }
}
