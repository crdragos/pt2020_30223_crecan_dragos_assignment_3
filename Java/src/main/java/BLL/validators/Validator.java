package BLL.validators;

/**
 * Interfata care contine metoda de validare
 * @param <T>
 */

public interface Validator<T> {

    public void validate(T t);
}
