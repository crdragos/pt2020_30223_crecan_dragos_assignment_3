package BLL.validators;

import Model.Customer;

import java.util.regex.Pattern;

/**
 * Aceasta clasa are rolul de valida numele clientului pe care utilizatorul inearca sa il introduca in baza de date
 * Validarea se va face prin compararea valorii pe care dorim sa o inseram cu un pattern, pentru a nu putea introduce caractere speciale sau cifre
 * Se verifica si lungimea numelui intodus, pentru a nu depasi lungimea acceptata in baza de date
 */

public class CustomerNameValidator implements Validator<Customer> {

    private static final String NAME_PATTERN = "^[a-z A-Z]+$";

    @Override
    public void validate(Customer customer) {
        if (customer.getName().length() <= 30) {
            Pattern pattern = Pattern.compile(NAME_PATTERN);
            if (!pattern.matcher(customer.getName()).matches()) {
                throw new IllegalArgumentException("Invalid customer name: special character and digits are not allowed");
            }
        } else {
            throw new IllegalArgumentException("Invalid customer name: too long");
        }
    }
}
