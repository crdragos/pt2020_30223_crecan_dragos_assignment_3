package BLL.validators;

import Model.OrderProduct;

/**
 * Aceasta clasa are rolul de valida cantitatea comandata dintr-un anumit produs pe care utilizatorul inearca sa o introduca in baza de date
 * Validarea se va face prin simpla verificare a cantitatii, aceasta trebuie neaparat sa fie mai mare ca 0
 */

public class OrderProductQuantityValidator implements Validator<OrderProduct> {
    @Override
    public void validate(OrderProduct orderProduct) {
        if (orderProduct.getQuantity() <= 0) {
            throw new IllegalArgumentException("Invalid order quantity: quantity must be greater then 0");
        }
    }
}
