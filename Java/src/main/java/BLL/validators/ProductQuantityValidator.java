package BLL.validators;

import Model.Product;

/**
 * Aceasta clasa are rolul de valida cantitatea disponibila a unui anumit produs pe care utilizatorul inearca sa o introduca in baza de date
 * Validarea se va face prin simpla verificare a cantitatii, aceasta trebuie neaparat sa fie mai mare ca 0
 */

public class ProductQuantityValidator implements Validator<Product> {

    @Override
    public void validate(Product product) {
        if (product.getQuantity() <= 0) {
            throw new IllegalArgumentException("Invalid product quantity: quantity must be greater then 0");
        }
    }
}
