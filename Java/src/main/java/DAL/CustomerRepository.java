package DAL;

import Model.Customer;

/**
 * Repository-ul clasei Client
 * Toate metodele vor fi mostenite din clasa abstracta, generica
 */

public class CustomerRepository extends AbstractRepository<Customer> {

}
