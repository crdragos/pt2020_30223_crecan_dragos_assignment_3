package DAL;

import Model.Product;

/**
 * Repository-ul clasei Product
 * Toate metodele vor fi mostenite din clasa abstracta, generica
 */

public class ProductRepository extends AbstractRepository<Product> {

}
