package DAL;

import Connection.ConnectionFactory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Un repository abstract pentru a evita scrierea aceluia cod in mai multe locuri
 * Aceasta clasa va fi mostenita de catre fiecare tabela din baza de date, in clasa corespunzatoare acestui repository
 * Rolul claselor din acest pachet este acela de a face legatura cu baza de date
 * @param <T>
 */

public abstract class AbstractRepository<T> {

    protected static final Logger LOGGER = Logger.getLogger(AbstractRepository.class.getName());
    protected final Class<T> type;

    protected AbstractRepository() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * Se creaza integrocarea de tip "SELECT * FOROM <TABLE_NAME>"
     * Se returneaza un strung care reprezinta interogarea, care va fi transmisa mai departe la metodele in care este nevoie sa aducem datele din tabele
     * @return
     */

    private String createSelectQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT");
        sb.append(" * ");
        sb.append("FROM ");
        sb.append(type.getSimpleName());

        return sb.toString();
    }

    /**
     * Se creaza interogarea de tip "SELECT * FROM <TABLE_NAME> WHERE field = ..."
     * Se primeste ca si parametru campul dupa care se face cautarea
     * Se teruneaza interogarea care va fi transmisa mai departe catre baza de date.
     * @param field
     * @return
     */

    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT");
        sb.append(" * ");
        sb.append("FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " = ?");

        return sb.toString();
    }

    /**
     * Se creaza conexiunea cu baza de date
     * Se creaza query-ul de tip "SELECT * FROM <TABLE_NAME>"
     * Se executa query-ul si se creaza obiectele ce vor fi returnate
     * Se returneaza o lista de obiecte din clasa T, unde clasa T este clasa care apeleaza metoda
     * @return
     */

    public ArrayList<T> get() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        String query = createSelectQuery();

        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();

            return (ArrayList<T>) createObjects(resultSet);
        } catch (SQLException sqlException) {
            LOGGER.log(Level.WARNING, type.getName() + "DAL : get => " + sqlException.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return null;
    }

    /**
     * Se creaza conexiunea cu baza de date
     * Se creaza query-ul de tip "SELECT * FROM <TABLE_NAME> WHERE idFromTable = id"
     * Se face cautarea in vaza de date dupa, id
     * Se executa query-ul si se creaza obiectele ce vor fi returnate
     * Metoda primeste ca si parametru valoarea filed-ului dupa care se face cautarea
     * Metoda retuneaza o instanta a clasei care apeleaza metoda, cea a carui id corespunde cu cel dat ca parametru, sau null in caz ca nu exista
     * @param id
     * @return
     */

    public T getById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        String query = createSelectQuery(type.getDeclaredFields()[0].getName());

        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            return createObjects(resultSet).get(0);
        } catch (SQLException sqlException) {
            LOGGER.log(Level.WARNING, type.getName() + "DAL : getById => " + sqlException.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return null;
    }

    /**
     * Se creaza conexiunea cu baza de date
     * Se creaza query-ul de tip "SELECT * FROM <TABLE_NAME> WHERE nameFromTable = name"
     * Cautarea in baza de date se va face dupa nume
     * Se executa query-ul si se creaza obiectele ce vor fi returnate
     * Se primeste ca si parametru numele pe care dorim sa il cautam in baza de date
     * Metoda returneaza o o instanta a clasei, cea care are numele cautat, in baza de date sau null in caz nu exista
     * @param name
     * @return
     */

    public T getByName(String name) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        String query = createSelectQuery(type.getDeclaredFields()[1].getName());

        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, name);
            resultSet = statement.executeQuery();

            List<T> list = createObjects(resultSet);
            if (list.size() == 0) {
                return null;
            } else {
                return list.get(0);
            }
        } catch (SQLException sqlException) {
            LOGGER.log(Level.WARNING, type.getName() + "DAL : getByName => " + sqlException.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return null;
    }

    /**
     * Se creeaza obiectele folosind ReflectionTehniques
     * Parametrul de intrare este reultatul obtinut in urma efecutauarii unuei query
     * Se returneaza obiectele create sub forma unei liste
     * @param resultSet
     * @return
     */

    private List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();

        try {
            while (resultSet.next()) {
                T instance = type.newInstance();

                for (Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }

                list.add(instance);

                if(list.size() == 0) {
                    throw new Exception("Size 0");
                }
            }
        } catch (InstantiationException instantiationException) {
            LOGGER.log(Level.WARNING, type.getName() + "DAL : Create objects => " + instantiationException.getMessage());
        } catch (IllegalAccessException illegalAccessException) {
            LOGGER.log(Level.WARNING, type.getName() + "DAL : Create objects => " + illegalAccessException.getMessage());
        } catch (IllegalArgumentException illegalArgumentException) {
            LOGGER.log(Level.WARNING, type.getName() + "DAL : Create objects =>" + illegalArgumentException.getMessage());
        } catch (InvocationTargetException invocationTargetException) {
            LOGGER.log(Level.WARNING, type.getName() + "DAL : Create objects => " + invocationTargetException.getMessage());
        } catch (SecurityException securityException) {
            LOGGER.log(Level.WARNING, type.getName() + "DAL : Create objects => " + securityException.getMessage());
        } catch (SQLException sqlException) {
            LOGGER.log(Level.WARNING, type.getName() + "DAL : Create objects => " + sqlException.getMessage());
        } catch (IntrospectionException introspectionException) {
            LOGGER.log(Level.WARNING, type.getName() + "DAL : Create objects => " + introspectionException.getMessage());
        } catch (Exception exception) {
            if(exception.getMessage().compareTo("Size 0") == 0) {
                System.out.println("error - size 0");
            }
        }

        return list;
    }

    /**
     * Se creaza un query pentru operatia de inserare in baza de date
     * Se transmite mai departe la metodele care fac inserari in baza de date
     * Parametrul de intrare are rolul de a specifica tabela in care se face inserarea (numele clasei) si de a spune ce valori vor fi inserate (atributele instantei)
     * Se returneaza query-ul creat, folosit la inserare
     * @param t
     * @return
     */

    private String createInsertQuery(T t) {
        StringBuilder sb = new StringBuilder();

        sb.append("INSERT INTO `");
        sb.append(type.getSimpleName() + "` (");

        boolean first = true;
        boolean second = true;

        for (Field field : type.getDeclaredFields()) {
            if (first) {
                first = false;
            } else if(second) {
                sb.append(field.getName());
                second = false;
            } else {
                sb.append(", " + field.getName());
            }
        }
        sb.append(") VALUES (");

        try {
            first = true;
            second = true;

            for (Field field : type.getDeclaredFields()) {
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                Method method = propertyDescriptor.getReadMethod();
                Object value = method.invoke(t);

                if (first) {
                    first = false;
                } else if(second) {
                    sb.append("'" + value.toString() + "'");
                    second = false;
                } else {
                    sb.append(", '" + value.toString() + "'");
                }
            }

        } catch (IllegalArgumentException illegalArgumentException) {
            illegalArgumentException.printStackTrace();
        } catch (IllegalAccessException illegalAccessException) {
            illegalAccessException.printStackTrace();
        } catch (IntrospectionException introspectionException) {
            introspectionException.printStackTrace();
        } catch (InvocationTargetException invocationTargetException) {
            invocationTargetException.printStackTrace();
        }

        sb.append(")");

        return sb.toString();
    }

    /**
     * Adauga un obiect al clasei T in baza de date, in tabela coresupuzatoare acestei clase
     * Parametrul reprezinta obicetul pe care dorim sa il inseram in baza de date
     * Se returneaza id-ul obiecylui inserat
     * @param t
     * @return
     */

    public int add(T t) {
        Connection connection = null;
        PreparedStatement statement = null;

        String query = createInsertQuery(t);

        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            statement.executeUpdate();

            ResultSet resultSet = statement.getGeneratedKeys();
            if(resultSet.next()) {
                return resultSet.getInt(1);
            }

        } catch (SQLException sqlException) {
            LOGGER.log(Level.WARNING, type.getName() + "DAL : add => " + sqlException.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return -1;
    }

    /**
     * Se creaza un query pentru operatia de update din baza de date
     * Se reapeleaza in metodele care fac update-uri in baza de date
     * Parametrul de intrare t, reprezinta noul obiect care va suprascrie un ibiect deja existent in baza de date
     * Parametru de intrare id, reprezinta id-ul obiectului ale carui atribute vor avea valoarea celui primit ca si parametru
     * Se returneaza query-ul creat, sub forma de string
     * @param t
     * @param id
     * @return
     */

    private String createUpdateQuery(T t, int id) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE " + type.getSimpleName() + " SET ");

        try {
            String idc = "";
            boolean first = true;

            for (Field field : type.getDeclaredFields()) {
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                Method method = propertyDescriptor.getReadMethod();
                Object value = method.invoke(t);

                if (first) {
                    sb.append(field.getName() + "='" + value.toString() + "'");
                    idc = field.getName() + "='" + id + "'";
                    first = false;
                } else {
                    sb.append(", " + field.getName() + "='" + value.toString() + "'");
                }
            }

            sb.append(" WHERE " + idc);
        } catch (IllegalArgumentException illegalArgumentException) {
            illegalArgumentException.printStackTrace();
        } catch (IllegalAccessException illegalAccessException) {
            illegalAccessException.printStackTrace();
        } catch (InvocationTargetException invocationTargetException) {
            invocationTargetException.printStackTrace();
        } catch (IntrospectionException introspectionException) {
            introspectionException.printStackTrace();
        }

        return sb.toString();
    }

    /**
     * Se stabilieste conexiunea cu baza de date
     * Se creaza query-ul de update
     * Se face update obiectului primit ca si prim parametru
     * Parametrul de intrare t, reprezinta noul obiect care va suprascrie un ibiect deja existent in baza de date
     * Parametru de intrare id, reprezinta id-ul obiectului ale carui atribute vor avea valoarea celui primit ca si parametru
     * Se returneaza statusul efectuarii operatiei
     * @param t
     * @param id
     * @return
     */

    public boolean update(T t, int id) {
        Connection connection = null;
        PreparedStatement statement = null;

        String query = createUpdateQuery(t, id);

        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException sqlException) {
            LOGGER.log(Level.WARNING, type.getName() + "DAL : update => " + sqlException.getMessage());
            return false;
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return true;
    }

    /**
     * Se creaza un query pentru operatia de delete
     * Parametrul de intrare reprezinta valoarea id-ului ibiectului pe care dorim sa il stergeem din baza de date
     * Se returneaza statusul efectuarii operatiei
     * @param id
     * @return
     */

    private String createDeleteQuery(int id) {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM " + type.getSimpleName() + " WHERE " + type.getDeclaredFields()[0].getName() + "=" + id);
        return sb.toString();
    }

    /**
     * Se creaza conexiunea cu baza de date
     * Se creaza query-ul de delete
     * Se executa operatia de delete in baza de date
     * Parametrul de intrare reprezinta valoarea id-ului ibiectului pe care dorim sa il stergeem din baza de date
     * Se returneaza statusul efectuarii operatiei
     * @param id
     * @return
     */

    public boolean delete(int id) {
        Connection connection = null;
        PreparedStatement statement = null;

        String query = createDeleteQuery(id);

        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException sqlException) {
            LOGGER.log(Level.WARNING, type.getName() + "DAL : delete =>" + sqlException.getMessage());
            return false;
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return true;
    }
}
