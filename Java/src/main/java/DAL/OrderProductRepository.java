package DAL;

import Model.OrderProduct;

/**
 * Repository-ul clasei OrderProduct
 * Toate metodele vor fi mostenite din clasa abstracta, generica
 */

public class OrderProductRepository extends AbstractRepository<OrderProduct> {

}
