package Connection;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Aceasta clasa are rolul de a stabili conexiunea cu baza de date
 */

public class ConnectionFactory {

    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/warehouse?useSSL=false";
    private static final String USER = "root";
    private static final String PASSWORD = "1919kyo1965";

    private static ConnectionFactory singleInstance = new ConnectionFactory();

    private ConnectionFactory() {
        try{
            Class.forName(DRIVER);
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }
    }

    /**
     * Se creaza conexiunea cu baza de date.
     * In cazul in care operatia nu se efectueaza cu succes se va arunca o exceptie, prinsa in clauza catch.
     * Se returneaa conexiuna cu baza de date
     * @return
     */

    private Connection createConnection() {
        Connection connection = null;

        try {
            connection = DriverManager.getConnection(DBURL, USER, PASSWORD);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        return connection;
    }

    /**
     * Se verifica conexiunea cu baza de date.
     * Se returneaza conexiuna cu baza de date.
     * @return
     */

    public static Connection getConnection() {
        return singleInstance.createConnection();
    }

    /**
     * Se inchide conexiunea primita ca si parametru.
     * In cazul in care rezultatul operatiei nu este cel asteptat se arunca o exceptie, prinsa in clauza catch.
     * @param connection
     */

    public static void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException sqlException) {
                LOGGER.log(Level.WARNING, "An error occured while trying to close the connection");
            }
        }
    }

    /**
     * Se inchide statement-ul primit ca si parametru.
     * In cazul in care rezultatul operatiei nu este cel asteptat se arunca o exceptie, prinsa in clauza catch.
     * @param statement
     */

    public static void close(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException sqlException) {
                LOGGER.log(Level.WARNING, "An error occured while trying to close the statement");
            }
        }
    }

    /**
     * Se inchide result set-ul primit ca si parametru.
     * In cazul in care rezultatul operatiei nu este cel asteptat se arunca o exceptie, prinsa in clausa catch.
     * @param resultSet
     */

    public static void close(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException sqlException) {
                LOGGER.log(Level.WARNING, "An error occrued while trying to close the ResultSet");
            }
        }
    }
}
